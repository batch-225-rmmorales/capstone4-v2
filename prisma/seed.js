import { PrismaClient } from '@prisma/client';
const prisma = new PrismaClient();

const modelNames = [
	'account',
	'cart',
	'message',
	'product',
	'session',
	'user',
	'verificationToken',
	'wallet'
];
const reset = async () => {
	// await prisma.message.deleteMany({});
	// await prisma.user.deleteMany({});
	// // // await prisma.account.deleteMany({});
	// // // await prisma.session.deleteMany({});
};

reset();
const seedUser = async (element) => {
	const user = await prisma.user.create({
		data: {
			email: element.email,
			name: element.firstName + ' ' + element.lastName,
			username: element.username,
			image: element.image
		}
	});
};
const seedUsers = async () => {
	const response = await fetch('https://dummyjson.com/users');
	const data = await response.json();
	data.users.forEach((element) => {
		seedUser(element);
	});
};
// seedUsers();
const seedProduct = async (element) => {
	const response = await fetch('https://dummyjson.com/users');
	const users = await response.json();
	var randomUser = users.users[Math.floor(Math.random() * users.users.length)];
	console.log(randomUser.username);

	const product = await prisma.product.create({
		data: {
			title: element.title,
			description: element.description,
			price: Number(element.price) * 60,
			thumbnail: element.thumbnail,
			images: element.images,
			category: element.category,
			seller: randomUser.username,
			stock: 10
		}
	});
};
const seedProducts = async () => {
	const response = await fetch('https://dummyjson.com/products?limit=100');
	const data = await response.json();
	data.products.forEach((element) => {
		seedProduct(element);
	});
};
seedProducts();
