import { prisma, dbConnect, dbDisconnect } from '$lib/database';
import { fail, redirect } from '@sveltejs/kit';

/** @type {import('./$types').Actions} */
export const actions = {
	default: async (event) => {
		let email;
		let password;
		await event.request.formData().then((data) => {
			email = data.get('email');
			password = data.get('password');
		});

		const user = await prisma.user.create({
			data: {
				email: email,
				password: password
			}
		});

		throw redirect(302, '/login');
	}
};
