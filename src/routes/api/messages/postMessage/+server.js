import { json } from '@sveltejs/kit';

import prisma from '$lib/prisma';

export const POST = async (event) => {
	const rawBody = await event.request.text();
	const body = JSON.parse(rawBody);
	// console.log(body);
	const message = await prisma.message.create({
		data: {
			msg: body.msg,
			msgTo: body.msgTo,
			msgFrom: body.msgFrom,
			imgFrom: body.imgFrom
		}
	});

	// messages = JSON.parse(JSON.stringify(messages));
	// messages = messages.reverse();
	return json(message);
};
