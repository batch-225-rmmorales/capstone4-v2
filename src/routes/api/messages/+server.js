import { json } from '@sveltejs/kit';

import prisma from '$lib/prisma';

export const GET = async (event) => {
	const messages = await prisma.message.findMany();

	// messages = JSON.parse(JSON.stringify(messages));
	// messages = messages.reverse();
	return json(messages);
};
