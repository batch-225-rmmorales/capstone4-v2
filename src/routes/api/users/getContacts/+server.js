import { json } from '@sveltejs/kit';
import prisma from '$lib/prisma';
let data = [
	{
		id: 1,
		firstName: 'Daniel',
		lastName: 'Padilla',
		email: 'daniel@padilla.com',
		phone: '+63 791 675 8914',
		username: 'atuny0',
		password: '9uQFF1Lh',

		image:
			'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSSiajAt0WAOWhJEEqBX4MTz7gx0UAoGOlpxZdb0EhQlkMAVvcxxSdDPgw&s',

		address: '1745 T Street Southeast'
	},
	{
		img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSSiajAt0WAOWhJEEqBX4MTz7gx0UAoGOlpxZdb0EhQlkMAVvcxxSdDPgw&s',
		title: 'Daniel Padilla',
		subtitle: 'Actor'
	}
];
export const GET = async (event) => {
	let username = event.url.searchParams.get('username');
	let chatwith = event.url.searchParams.get('chatwith');
	console.log(event);
	const messages = await prisma.message.findMany({
		where: {
			OR: [{ msgTo: username }, { msgFrom: username }]
		}
	});
	let msgTo = messages.map((a) => a.msgTo);
	let msgFrom = messages.map((a) => a.msgFrom);
	let usersWithChatHistory = msgTo.concat(msgFrom);
	let usersWithChatHistory2 = usersWithChatHistory.filter((a) => a !== username);
	usersWithChatHistory2.push(chatwith);
	const users = await prisma.user.findMany({
		where: {
			username: { in: usersWithChatHistory2 }
		}
	});
	return json(users);
};

// const ret = await prisma.signe.findMany({
// 	where: {
// 		id: { in: [1, 2, 12] },
// 	}
// })
