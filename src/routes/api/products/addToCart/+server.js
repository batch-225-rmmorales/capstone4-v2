import { json } from '@sveltejs/kit';

import prisma from '$lib/prisma';

export const POST = async (event) => {
	const rawBody = await event.request.text();
	const body = JSON.parse(rawBody);
	// find if seller/buyer pair exist
	console.log(body);

	const findCart = await prisma.cart.findMany({
		where: {
			AND: [{ buyer: body.buyer }, { seller: body.seller }, { isPaid: false }]
		}
	});

	if (findCart.length > 0) {
		const uniqueCart = await prisma.cart.findUnique({
			where: {
				id: findCart[0].id
			}
		});
		let inserted = false;
		uniqueCart.products.forEach((product) => {
			if (product.id == body.products[0].id) {
				product.quantity = String(Number(product.quantity) + Number(body.products[0].quantity));
				inserted = true;
			}
		});
		if (!inserted) {
			uniqueCart.products.push(body.products[0]);
		}
		let totalProducts = uniqueCart.products.length;
		let totalAmount = uniqueCart.products.reduce(
			(r, { price, quantity }) => (r += Number(price) * Number(quantity)),
			0
		);
		let totalQuantity = uniqueCart.products.reduce((r, { quantity }) => (r += Number(quantity)), 0);
		const updateCart = await prisma.cart.update({
			where: {
				id: findCart[0].id
			},
			data: {
				products: uniqueCart.products,
				totalProducts: totalProducts,
				total: totalAmount,
				totalQuantity: totalQuantity
			}
		});

		return json({ message: 'item added to existing cart', data: uniqueCart });
	} else {
		const cart = await prisma.cart.create({
			data: {
				seller: body.seller,
				buyer: body.buyer,
				products: body.products
			}
		});
		return json({ message: 'new cart created', data: cart });
	}

	// messages = JSON.parse(JSON.stringify(messages));
	// messages = messages.reverse();
};
