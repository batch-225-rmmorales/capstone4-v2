import { json } from '@sveltejs/kit';

import prisma from '$lib/prisma';

export const GET = async (event) => {
	let seller = event.url.searchParams.get('seller');
	let category = event.url.searchParams.get('category');
	let products;
	if (seller) {
		products = await prisma.product.findMany({
			where: { seller: seller }
		});
	} else if (category) {
		products = await prisma.product.findMany({
			where: { category: category }
		});
	} else {
		products = await prisma.product.findMany();
	}
	return json(products);
};
