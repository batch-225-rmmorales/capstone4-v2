import { PrismaClient } from '@prisma/client';
export const prisma = new PrismaClient();

export const dbConnect = async () => {
	await prisma.$connect();
};
export const dbDisconnect = async () => {
	await prisma.$disconnect();
};
